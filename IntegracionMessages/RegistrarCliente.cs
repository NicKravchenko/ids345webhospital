﻿using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionMessages
{
    public class RegistrarCliente : ICommand
    {
        public int UsuarioId { get; set; }
        public string Usuario { get; set; }
        public int Contraseña { get; set; }

    }
}
