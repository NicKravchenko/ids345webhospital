﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreHospital
{
    internal class Especialidad
    {
        public int ID_Especialidad { get; set; }

        public string Nombre_Especialidad { get; set; }

    }
}
