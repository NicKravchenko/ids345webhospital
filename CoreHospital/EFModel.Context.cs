﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoreHospital
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class HOSPITALCOREPFEntities2 : DbContext
    {
        public HOSPITALCOREPFEntities2()
            : base("name=HOSPITALCOREPFEntities2")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Doctor> Doctor { get; set; }
        public DbSet<Especialidad> Especialidad { get; set; }
        public DbSet<Factura> Factura { get; set; }
        public DbSet<MetodoPago> MetodoPago { get; set; }
        public DbSet<Paciente> Paciente { get; set; }
    }
}
