﻿using CoreHospital.DataSet1TableAdapters;
using NServiceBus;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreHospital
{
    public partial class Program
    {
        static  void Main()
        {
            string user, password;
            Console.Write("Please log in. \n \n");
            do
            {
                Console.Write("Digite el User: ");
                user = Console.ReadLine();
                Console.Write("Digite la contraseña: ");
                password = Console.ReadLine();

                if ((user != "admin" || password != "admin"))
                    Console.WriteLine("Login Error");
                else
                {
                    Console.WriteLine("Login successful");
                }
            }
            while (user != "admin" || password != "admin");
            {
                Console.WriteLine($"E - insertar especialidad, D - Insertar doctor");

                var key = Console.ReadKey();
                Console.WriteLine("\n");

                switch (key.Key)
                {

                    case ConsoleKey.D:
                        {
                            InsertDoctor();
                        }

                        break;

                    case ConsoleKey.E:
                        {
                            InsertarEspecialidad();
                        }

                        break;

                    case ConsoleKey.Q:
                        return;

                    default:
                        break;

                }

            }


        }
        static void InsertDoctor()
        {
            DoctorTableAdapter doctorTableAdapter = new DoctorTableAdapter();

            Console.WriteLine("Nombre");
            var nombre = Console.ReadLine();
            Console.WriteLine("Apellido");
            var apellido = Console.ReadLine();

            var d = new Doctor()
            {
                Nombre = nombre,
                Apellido = apellido,
                Cedula = "001205302",
                Direccion = "La fe",
                Telefono = "8498987821",
                ID_Especialidad = 2,
                Fecha_Nacimiento = DateTime.Parse("1995/07/12")
            };

            doctorTableAdapter.Insert(d.Nombre, d.Apellido, d.Cedula, d.Direccion, d.Telefono, d.ID_Especialidad, d.Fecha_Nacimiento);

        }
        private static void InsertarEspecialidad()
        {
            EspecialidadTableAdapter especialidadTableAdapter = new EspecialidadTableAdapter();

            Console.WriteLine("Nombre");
            var nombre = Console.ReadLine();

            var E = new Especialidad()
            {
                ID_Especialidad = 1,
                Nombre_Especialidad = nombre
            };
            especialidadTableAdapter.Insert(E.Nombre_Especialidad);
        }
    }
}
