﻿using System;

namespace CoreHospital
{
    public partial class Program
    {
        public class Doctor
        {
            public string Nombre { get; set; }
            public string Apellido { get; set; }
            public string Cedula { get; set; }
            public string Direccion { get; set; }
            public string Telefono { get; set; }
            public int ID_Especialidad { get; set; }

            public DateTime Fecha_Nacimiento { get; set; }

        };
    }
}
