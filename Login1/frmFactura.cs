﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login1
{
    public partial class frmFactura : Form
    {
        public frmFactura()
        {
            InitializeComponent();
        }

        private void frmFactura_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.BuscarUltimaFactura' table. You can move, or remove it, as needed.
            this.buscarUltimaFacturaTableAdapter.Fill(this.dataSet1.BuscarUltimaFactura);

            this.reportViewer1.RefreshReport();
        }
    }
}
