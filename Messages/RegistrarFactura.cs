﻿using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messages
{
    public class RegistrarFactura : ICommand
    {

        public string Descripcion { get; set; }
        public float Precio { get; set; }
        public string Articulo { get; set; }
        public float Subtotal { get; set; }
        public float Total { get; set; }
        public float tax { get; set; }
        public DateTime Fecha { get; set; }
        public float PrecioTotal { get; set; }
        public int ID_MetodoPago { get; set; }

    }
}
