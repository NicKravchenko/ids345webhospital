﻿using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messages
{
    public class RegistrarDoctor : ICommand
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; } 
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public int ID_Especialidad { get; set; }
        public DateTime Fecha_Nacimiento { get; set; }

    }
}
