﻿using IntegracionMessages;
using NServiceBus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            
         }
        public static async Task Main()
        {
            Console.Title = "Web";

            var endpointConfiguration = new EndpointConfiguration("Web");

            var transport = endpointConfiguration.UseTransport<LearningTransport>();
            var routing = transport.Routing();
            routing.RouteToEndpoint(typeof(RegistrarCliente), "Core");

            var endpointInstance = await Endpoint.Start(endpointConfiguration).ConfigureAwait(false);


            int UsuarioId = 123, Contraseña = 2323;
            String Usuario = "sd";

            var command = new RegistrarCliente
            {
                UsuarioId = UsuarioId,
                Usuario = Usuario,
                Contraseña = Contraseña
            };

            await endpointInstance.Send(command).ConfigureAwait(false);

            await endpointInstance.Stop().ConfigureAwait(false);

        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Main();

        }
    }
}
