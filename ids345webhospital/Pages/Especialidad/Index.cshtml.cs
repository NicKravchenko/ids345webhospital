﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MigrationExample.Models;
using WebHospital.Data;

namespace WebHospital.Pages.Especialidad
{
    [Authorize(Policy = "AdminPolicy")]
    public class IndexModel : PageModel
    {
        private readonly WebHospital.Data.HospitalDbContext _context;

        public IndexModel(WebHospital.Data.HospitalDbContext context)
        {
            _context = context;
        }

        public IList<MigrationExample.Models.Especialidad> Especialidad { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Especialidades != null)
            {
                Especialidad = await _context.Especialidades.ToListAsync();
            }
        }
    }
}
