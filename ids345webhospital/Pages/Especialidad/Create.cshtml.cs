﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MigrationExample.Models;
using WebHospital.Data;

namespace WebHospital.Pages.Especialidad
{
    [Authorize(Policy = "AdminPolicy")]
    public class CreateModel : PageModel
    {
        private readonly WebHospital.Data.HospitalDbContext _context;

        public CreateModel(WebHospital.Data.HospitalDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public MigrationExample.Models.Especialidad Especialidad { get; set; }
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Especialidades.Add(Especialidad);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
