﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MigrationExample.Models;
using WebHospital.Data;

namespace WebHospital.Pages.Especialidad
{
    [Authorize(Policy = "AdminPolicy")]
    public class DeleteModel : PageModel
    {
        private readonly WebHospital.Data.HospitalDbContext _context;

        public DeleteModel(WebHospital.Data.HospitalDbContext context)
        {
            _context = context;
        }

        [BindProperty]
      public MigrationExample.Models.Especialidad Especialidad { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Especialidades == null)
            {
                return NotFound();
            }

            var especialidad = await _context.Especialidades.FirstOrDefaultAsync(m => m.Id == id);

            if (especialidad == null)
            {
                return NotFound();
            }
            else 
            {
                Especialidad = especialidad;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null || _context.Especialidades == null)
            {
                return NotFound();
            }
            var especialidad = await _context.Especialidades.FindAsync(id);

            if (especialidad != null)
            {
                Especialidad = especialidad;
                _context.Especialidades.Remove(Especialidad);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
