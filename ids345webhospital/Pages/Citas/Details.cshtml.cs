﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebHospital.Data;
using WebHospital.Models;

namespace WebHospital.Pages.Cita
{
    [Authorize(Policy = "PacientePolicy")]
    public class DetailsModel : PageModel
    {
        private readonly WebHospital.Data.HospitalDbContext _context;

        public DetailsModel(WebHospital.Data.HospitalDbContext context)
        {
            _context = context;
        }

      public Models.Cita Cita { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Citas == null)
            {
                return NotFound();
            }

            var cita = await _context.Citas.FirstOrDefaultAsync(m => m.Id == id);
            if (cita == null)
            {
                return NotFound();
            }
            else 
            {
                Cita = cita;
            }
            return Page();
        }
    }
}
