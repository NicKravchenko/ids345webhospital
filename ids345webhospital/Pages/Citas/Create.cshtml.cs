﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebHospital.Data;
using WebHospital.Models;

namespace WebHospital.Pages.Cita
{
    [Authorize(Policy = "PacientePolicy")]
    public class CreateModel : PageModel
    {
        private readonly WebHospital.Data.HospitalDbContext _context;

        public CreateModel(WebHospital.Data.HospitalDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Models.Cita Cita { get; set; }
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid)
            {
                return Page();
            }

            Cita.FechaCita = DateTime.Parse(Cita.FechaCita.ToString() + "00:00:00.0000000:");
            _context.Citas.Add(Cita);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
