﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebHospital.Data;
using WebHospital.Models;

namespace WebHospital.Pages.Cita
{
    [Authorize(Policy = "PacientePolicy")]
    public class IndexModel : PageModel
    {
        private readonly WebHospital.Data.HospitalDbContext _context;

        public IndexModel(WebHospital.Data.HospitalDbContext context)
        {
            _context = context;
        }

        public IList<Models.Cita> Cita { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Citas != null)
            {
                Cita = await _context.Citas.ToListAsync();
            }
        }
    }
}
