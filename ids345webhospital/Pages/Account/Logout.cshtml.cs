using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebHospital.Models;

namespace WebHospital.Pages.Account
{
    public class LogoutModel : PageModel
    {
        public UserManager<UserHospitalClient> userManager { get; }

        public SignInManager<UserHospitalClient> signInManager { get; }
        private IConfiguration _configuration;

        public LogoutModel(SignInManager<UserHospitalClient> signInManager,
            IConfiguration configuration,
            UserManager<UserHospitalClient> userManager)
        {
            this.signInManager = signInManager;
            _configuration = configuration;
            this.userManager = userManager;
        }
        public async Task<IActionResult> OnPostAsync()
        {
            //string AdminCookieName = _configuration.GetValue<string>("AdminCookieAuthName");
            var userWithMail = await userManager.FindByEmailAsync("sdsd");
            
            await signInManager.SignOutAsync();
            return RedirectToPage("/Account/Login");
        }
    }
}
