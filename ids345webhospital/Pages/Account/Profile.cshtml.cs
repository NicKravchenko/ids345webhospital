using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebHospital.Models;

namespace WebHospital.Pages.Account
{
    [Authorize(Policy = "PacientePolicy")]

    public class ProfileModel : PageModel
    {
        private readonly UserManager<UserHospitalClient> userManager;

        public ProfileModel(UserManager<UserHospitalClient> userManager)
        {
            this.userManager = userManager;
        }
        [BindProperty]
        public UserHospitalClient UserModel { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await userManager.FindByNameAsync(User.Identity.Name);
            UserModel = user;
            return Page();
        }
    }
}
