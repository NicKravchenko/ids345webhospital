using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using WebHospital.Models;
using System.ComponentModel;
using System.Security.Claims;
using NServiceBus;

namespace WebHospital.Pages.Account
{
    public class RegisterModel : PageModel
    {
        public UserManager<UserHospitalClient> userManager { get; }


        public RegisterModel(UserManager<UserHospitalClient> userManager)
        {
            this.userManager = userManager;
        }

        [BindProperty]
        public RegisterViewModel RegisterViewModel { get; set; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var user = new UserHospitalClient
            {
                UserName = RegisterViewModel.Email,
                Email = RegisterViewModel.Email,
                PhoneNumber = RegisterViewModel.PhoneNumber,

                Nombre = RegisterViewModel.Nombre,
                Apellido = RegisterViewModel.Apellido,

                TipoDocumentoId = RegisterViewModel.TipoDocumentoId,
                Documento = RegisterViewModel.Documento,

                Pais =  RegisterViewModel.Pais,
                Ciudad =   RegisterViewModel.Ciudad,
                Direccion = RegisterViewModel.Direccion,
            };

            var result = await this.userManager.CreateAsync(user, RegisterViewModel.Password);

            var userWithMail = await this.userManager.FindByEmailAsync(RegisterViewModel.Email);

            var claimDepartment = new Claim("PacienteClaim", "PacienteClaim"); 

            if (result.Succeeded)
            {
                //WebIntegrationHelper.Program.InsertToCore(user.Nombre, user.Apellido, user.TipoDocumentoId, user.Documento,
                //    user.Pais, user.Ciudad, user.Direccion, user.PhoneNumber, user.Email);

                await this.userManager.AddClaimAsync(user, claimDepartment);
                return RedirectToPage("/Account/Login");
            }
            
            else
            {
                if (userWithMail != null)
                {
                    //adding error message to ModelState
                    ModelState.AddModelError("", $"{RegisterViewModel.Email.ToString()} ya est� ocupado.");

                }

                return Page();
            }
        }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage="Invalid email adress")]
        public string Email { get; set; }

        [Required]
        [DataType(dataType:DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        [MaxLength(100)]
        public string Nombre { get; set; }

        [Required]
        [MaxLength(100)]
        public string Apellido { get; set; }

        [Required]
        [DisplayName("Tipo de documento")]
        [Range(1, 2, ErrorMessage = "Tipo de documento incorrecto")]
        public int TipoDocumentoId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Documento { get; set; }

        [Required]
        [MaxLength(40)]
        public string Pais { get; set; }

        [Required]
        [MaxLength(50)]
        public string Ciudad { get; set; }

        [Required]
        [MaxLength(200)]
        public string Direccion { get; set; }
    }
}
