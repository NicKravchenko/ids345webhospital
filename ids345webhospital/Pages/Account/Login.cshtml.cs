using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using WebHospital.Models;

namespace WebHospital.Pages.Account
{
    public class LoginModel : PageModel
    {
        public SignInManager<UserHospitalClient> signInManager { get; }
        private IConfiguration _configuration;

        public LoginModel(SignInManager<UserHospitalClient> signInManager,
            IConfiguration configuration)
        {
            this.signInManager = signInManager;
            _configuration = configuration;
        }

        [BindProperty]
        public CredentialsViewModel CredentialVM { get; set; }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            
            var result = await signInManager.PasswordSignInAsync(this.CredentialVM.Email,
                this.CredentialVM.Password,
                this.CredentialVM.RememberMe,
                false);

            if (result.Succeeded)
                return RedirectToPage("/Index");
            else
            {
                if (result.IsLockedOut)
                {
                    ModelState.AddModelError("Login", "You are locked out.");
                }
                else
                {
                    ModelState.AddModelError("Login", "Failde to log in");
                }

                return Page();
            }
        }
    }
    public class CredentialsViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        public bool RememberMe { get; set; }
    }
}
