﻿using WebHospital.Data;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace WebHospital.Pages
{
    public class IndexModel : PageModel
    {
        private readonly HospitalDbContext _context;

        public IndexModel(HospitalDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Models.Cita Cita { get; set; }

        public IActionResult OnGet()
        {
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            //if (!ModelState.IsValid)
            //{
            //    return Page();
            //}

            _context.Citas.Add(Cita);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
