﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MigrationExample.Models;
using WebHospital.Data;

namespace WebHospital.Pages.Doctores
{
    [Authorize(Policy = "AdminPolicy")]
    public class IndexModel : PageModel
    {
        private readonly WebHospital.Data.HospitalDbContext _context;

        public IndexModel(WebHospital.Data.HospitalDbContext context)
        {
            _context = context;
        }

        public IList<Doctor> Doctor { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Doctores != null)
            {
                Doctor = await _context.Doctores
                .Include(d => d.Especialidad).ToListAsync();
            }
        }
    }
}
