﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MigrationExample.Models;
using WebHospital.Data;

namespace WebHospital.Pages.Doctores
{
    [Authorize(Policy = "AdminPolicy")]
    public class CreateModel : PageModel
    {
        private readonly WebHospital.Data.HospitalDbContext _context;

        public CreateModel(WebHospital.Data.HospitalDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["EspecialidadId"] = new SelectList(_context.Especialidades, "Id", "NombreEspecialidad");
            return Page();
        }

        [BindProperty]
        public Doctor Doctor { get; set; }
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Doctores.Add(Doctor);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
