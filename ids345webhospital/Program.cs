using WebHospital.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NServiceBus;
using Microsoft.AspNetCore.Hosting; //<-- Here it is
using Microsoft.Extensions.Hosting;
using WebHospital.Data;
using Microsoft.AspNetCore.Authorization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();

//Db context for hospital database
builder.Services.AddDbContext<HospitalDbContext>(options => options.
    UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

//Db context for users database
//builder.Services.AddDbContext<AppIdentityDbContext>(options => options.
//    UseSqlServer(builder.Configuration.GetConnectionString("AppIdentityConnection")));

builder.Services.AddIdentity<UserHospitalClient, IdentityRole>(options =>
{
    options.Password.RequiredLength = 4;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequireLowercase = false;
    options.Password.RequireUppercase = false;
    options.Password.RequireDigit = false;
    options.Password.RequireNonAlphanumeric = false;

    options.Lockout.MaxFailedAccessAttempts = 5;
    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(1);

    options.User.RequireUniqueEmail = true;
}).AddEntityFrameworkStores<HospitalDbContext>();

builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = "/Account/Login";
    options.AccessDeniedPath = "/Account/AccessDenied";
});



builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("PacientePolicy",
        policy => policy.RequireClaim("PacienteClaim").Build());

    options.AddPolicy("AdminPolicy",
        policy => policy.RequireClaim("AdminClaim").Build());
});



var app = builder.Build();
//Add-Migration CreateUsersDatabase -context AppIdentityDbContext
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages();

app.Run();
