using WebHospital.Models;
using Microsoft.EntityFrameworkCore;
using MigrationExample.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace WebHospital.Data
{
    public class HospitalDbContext : IdentityDbContext<UserHospitalClient>
    {
        public DbSet<Doctor> Doctores { get; set; }
        public DbSet<Especialidad> Especialidades { get; set; }
        public DbSet<Cita> Citas { get; set; }

        public HospitalDbContext(DbContextOptions<HospitalDbContext> options) : base(options)
        {
            
        }


    }
}
