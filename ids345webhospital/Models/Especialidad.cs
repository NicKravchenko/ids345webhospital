﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MigrationExample.Models
{
    public class Especialidad
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string NombreEspecialidad { get; set; }

        
        public ICollection<Doctor> Doctor { get; set; }

    }
}