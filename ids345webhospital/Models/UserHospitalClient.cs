﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHospital.Models
{
    public class UserHospitalClient : IdentityUser
    {
        [Required]
        [MaxLength(100)]
        public string Nombre { get; set; }

        [Required]
        [MaxLength(100)]
        public string Apellido { get; set; }

        public int TipoDocumentoId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Documento { get; set; }

        [Required]
        [MaxLength(40)]
        public string Pais { get; set; }

        [Required]
        [MaxLength(50)]
        public string Ciudad { get; set; }

        [Required]
        [MaxLength(200)]
        public string Direccion { get; set; }

    }
}
