﻿using System.ComponentModel.DataAnnotations;
using MigrationExample.Models;

namespace WebHospital.Models;


public class Cita
{
    
    public int Id { get; set; }

    [Required]
    public string Nombre { get; set; }

    [Required]
    public string Apellido { get; set; }

    [Required]
    public DateTime FechaCita { get; set; }

    [Required]
    public string Mensaje { get; set; }

}