﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHospital.Models;

namespace MigrationExample.Models
{
    public class Doctor
    {
        public int Id { get; set; }


        [Required(ErrorMessage = "El campo nombre es necesario")]
        [MaxLength(100)]
        public string Nombre { get; set; }


        [Required]
        [MaxLength(100)]
        public string Apellido { get; set; }

        [Required]
        [MaxLength(11)]
        public string Cedula { get; set; }


        [Required]
        [MaxLength(200)]
        public string Direccion { get; set; }


        [Required]
        [RegularExpression(@"^([0-9]{10})$")]
        [Column(TypeName = "varchar(13)")]
        public string Telefono { get; set; }

        
        public int EspecialidadId { get; set; }


        [Required]
        public DateTime FechaDeNacimiento { get; set; }

        
        public Especialidad Especialidad { get; set; }

    }
}
